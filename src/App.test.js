import React from "react";
import { mount, shallow } from "enzyme";
import App from "./App";
import each from "jest-each";

// Syntactic sugar, see: https://github.com/facebook/jest/issues/2157#issuecomment-279171856
// something like this will maybe added to the Jest API
function flushPromises() {
  return new Promise((resolve) => setImmediate(resolve));
}

describe("Test component methods", () => {
  it("test fetch default form not update state", () => {
    const componentUnderTest = shallow(<App />);
    const { schema, uiSchema } = componentUnderTest
      .instance()
      .fetchProjectForm(false);
    expect(uiSchema).toBeDefined();
    expect(schema).toBeDefined();
  });

  it("test fetch project form update state", () => {
    const componentUnderTest = shallow(<App />);
    const {
      schema,
      uiSchema,
    } = componentUnderTest.instance().fetchProjectBranchForm("project1", true);
    expect(uiSchema).toBeDefined();
    expect(schema).toBeDefined();
    return flushPromises().then(() => {
      expect(componentUnderTest.state().project).toEqual("project1");
    });
  });

  it("test fetch project form not update state", () => {
    const componentUnderTest = shallow(<App />);
    const {
      schema,
      uiSchema,
    } = componentUnderTest.instance().fetchProjectBranchForm("project1", false);
    expect(uiSchema).toBeDefined();
    expect(schema).toBeDefined();
    return flushPromises().then(() => {
      expect(componentUnderTest.state().project).toBeNull();
    });
  });

  it("test fetch project branch form (.bod-ui.yml)", () => {
    const componentUnderTest = shallow(<App />);
    componentUnderTest.instance().fetchForm("project1", "branch1");
    return flushPromises().then(() => {
      expect(componentUnderTest.state().error).toEqual(null);
      expect(componentUnderTest.state().uiSchema).toBeDefined();
      expect(componentUnderTest.state().schema).toBeDefined();
      expect(componentUnderTest.state().project).toEqual("project1");
      expect(componentUnderTest.state().branch).toEqual("branch1");
      expect(componentUnderTest.state().uiConfig).toEqual(".bod-ui.yml");
    });
  });

  it("test fetch project branch form (.another-ui.yml)", () => {
    const componentUnderTest = shallow(<App />);
    componentUnderTest
      .instance()
      .fetchForm("project1", "branch1", ".another-ui.yml");
    return flushPromises().then(() => {
      expect(componentUnderTest.state().error).toEqual(null);
      expect(componentUnderTest.state().uiSchema).toBeDefined();
      expect(componentUnderTest.state().schema).toBeDefined();
      expect(componentUnderTest.state().project).toEqual("project1");
      expect(componentUnderTest.state().branch).toEqual("branch1");
      expect(componentUnderTest.state().uiConfig).toEqual(".another-ui.yml");
    });
  });

  it("test query parameters project", () => {
    delete window.location;
    window.location = "bod?project=project1";

    const componentUnderTest = shallow(<App />);
    return flushPromises().then(() => {
      expect(componentUnderTest.state().error).toBeNull();
      expect(componentUnderTest.state().uiSchema).toBeDefined();
      expect(componentUnderTest.state().schema).toBeDefined();
      expect(componentUnderTest.state().project).toEqual("project1");
      expect(componentUnderTest.state().branch).toBeNull();
      expect(componentUnderTest.state().uiConfig).toEqual(".bod-ui.yml");
    });
  });
});

describe("Load Form using query parameters", () => {
  each([
    ["bod", null, null, ".bod-ui.yml"],
    ["bod?project=project1", "project1", null, ".bod-ui.yml"],
    [
      "bod?project=project1&branch=branch1",
      "project1",
      "branch1",
      ".bod-ui.yml",
    ],
    [
      "bod?project=project1&branch=branch1",
      "project1",
      "branch1",
      ".bod-ui.yml",
    ],
    [
      "bod?project=project1&branch=branch1&uiConfig=another-ui.yml",
      "project1",
      "branch1",
      "another-ui.yml",
    ],
  ]).it("when url is '%s'", (url, project, branch, uiConfig) => {
    delete window.location;
    window.location = url;

    const componentUnderTest = shallow(<App />);
    return flushPromises().then(() => {
      expect(componentUnderTest.state().error).toBeNull();
      expect(componentUnderTest.state().uiSchema).toBeDefined();
      expect(componentUnderTest.state().schema).toBeDefined();
      expect(componentUnderTest.state().project).toEqual(project);
      expect(componentUnderTest.state().branch).toEqual(branch);
      expect(componentUnderTest.state().uiConfig).toEqual(uiConfig);
    });
  });
});
